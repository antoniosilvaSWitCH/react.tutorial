import React from 'react';
import ReactDOM from 'react-dom';
import Car from './App.js';

class Garage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {show: true, favoriteHeader: "This garage is shite !"};
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({favoriteHeader: "Fuck, someone closed the garage doors !"})
        }, 2000)
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        document.getElementById("header2").innerHTML =
            "Before the update, the header was " + prevState.favoriteHeader;
    }

    componentDidUpdate() {
        document.getElementById("header3").innerHTML =
            "Then it changed to: " + this.state.favoriteHeader;
    }

    killCar = () => {
        this.setState({show: false});
    };

    render() {
        let myCar;

        if (this.state.show) {
            myCar = <Car model="Mustang" year="1954" color="red"/>;
        }

        return (
            <div>
                <div id>
                    <h1 id="header1">{this.state.favoriteHeader}</h1>
                    <h2>Who dares enter my domain !?</h2>
                    {myCar}
                </div>

                <div id>
                    <h2 id="header2"/>
                    <h2 id="header3"/>
                </div>

                <div>
                    <button
                        type="button"
                        onClick={this.killCar}>Kill intruder
                    </button>
                </div>

            </div>
        );
    }
}

ReactDOM.render(<Garage/>, document.getElementById('root'));