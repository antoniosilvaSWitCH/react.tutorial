import React from 'react';

class Car extends React.Component {
    constructor(props) {
        super(props);
        this.state = props;
    }

    randomYear = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    changeYear = () => {
        this.setState({year: this.randomYear(1950, 2020)});
    };

    componentWillUnmount() {
        alert("You are about to become a killer! Proceed?");
    }

    render() {
        return (
            <div>
                <h3>Hello, I am a Car with model {this.state.model} from {this.state.year} and
                    color {this.state.color}!</h3>
                <button
                    type="button"
                    onClick={this.changeYear}
                >Change year
                </button>
            </div>
        )
    }
}

export default Car;